**Alien Invasion**

My repository to practice Git while following along Chapters 12, 13 and 14 in [Python Crash Course, 2nd Edition by Eric Matthes](https://nostarch.com/pythoncrashcourse2e).

*Let's go*

---

## Chapter 12 – A ship that fires bullets

Chapter content

1. Set up **PyGame**.
2. Create a **rocket ship that moves right and left**.
3. Let rocket ship **fire bullets in response to player input**.